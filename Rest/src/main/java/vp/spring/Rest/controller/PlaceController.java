package vp.spring.Rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.Rest.model.Country;
import vp.spring.Rest.model.Place;
import vp.spring.Rest.service.CountryService;
import vp.spring.Rest.service.PlaceService;

@RestController
public class PlaceController {

	@Autowired
	PlaceService placeService;
	
	@Autowired
	CountryService countryService;
	
	@RequestMapping(value = "api/places", method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Place>> getAllPlaces(){
		List<Place> places = placeService.findAll();
		return new ResponseEntity<List<Place>>(places, HttpStatus.OK);
	}

	@RequestMapping(value = "api/places/{id}", method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Place> get(@PathVariable int id){
		Place place = placeService.findOne(id);
		return new ResponseEntity<Place>(place, HttpStatus.OK);
	}
	
	@RequestMapping(value = "api/places", method = RequestMethod.POST, consumes = 
			MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<Place> create(@RequestBody Place place) {
		
		Place retVal = placeService.save(place, place.getCountry().getId());
		
		return new ResponseEntity<>(retVal, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "api/places/{id}", method = RequestMethod.PUT, consumes = 
			MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<Place> update(@PathVariable int id, @RequestBody Place place) {
		place.setId(id);
		Place retVal = placeService.save(place, place.getCountry().getId());
		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}
	
	@RequestMapping(value = "api/places/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable int id) {
		Place place = placeService.findOne(id);
		if (place != null) {
			placeService.remove(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value ="api/places", method = RequestMethod.GET, params = "name")
	public ResponseEntity<List<Place>> getPlacesByName(@RequestParam String name) {
		List<Place> places = placeService.findByNameContains(name);
		
		return new ResponseEntity<>(places, HttpStatus.OK);
	}
	
	@RequestMapping(value = "api/places", method = RequestMethod.GET, params = "zipCode")
	public ResponseEntity<Place> getPlaceByZipCode(@RequestParam int zipCode) {
		Place place = placeService.findByZipCode(zipCode);
		
		return new ResponseEntity<>(place, HttpStatus.OK);
	}
	
	@RequestMapping(value = "api/places", method = RequestMethod.GET, params = "country")
	public ResponseEntity<List<Place>> getPlacesByCountry(@RequestParam String country) {
		List<Country> countries = countryService.findByName(country);
		List<Place> places = new ArrayList<Place>();
		for (Country c: countries ) {
			places.addAll(placeService.findByCountryId(c.getId()));
		}
		
		return new ResponseEntity<>(places, HttpStatus.OK);
	}
	

}
