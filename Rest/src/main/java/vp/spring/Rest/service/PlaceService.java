package vp.spring.Rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.spring.Rest.data.PlaceRepository;
import vp.spring.Rest.model.Place;

@Component
public class PlaceService {
	
	@Autowired
	PlaceRepository placeRepository;

	public List<Place> findAll(){
		return placeRepository.findAll();
	}
	
	public Place findOne(int id) {
		return placeRepository.findOne(id);
	}
	
	public List<Place> findByCountryId(int id){
		return placeRepository.findByCountryId(id);
	}
	
	public Place save(Place place, int countryId) {
		return placeRepository.savePlace(place, countryId);
	}
	
	public void remove(int id) {
	    placeRepository.deletePlace(id);
	}
	
	public List<Place> findByNameContains(String name) {
		return placeRepository.findByNameContains(name);
	}
	
	public Place findByZipCode(int zipCode) {
		return placeRepository.findByZipCode(zipCode);
	}
}
