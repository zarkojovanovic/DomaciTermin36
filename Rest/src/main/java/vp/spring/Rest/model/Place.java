package vp.spring.Rest.model;

public class Place {

	private int id;

	private String name;
	
	private int zipCode;
	
	private Country country;
	
	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Place() {
		super();
	}
	
	public Place(int id, String name, int zipCode, Country country) {
		super();
		this.id = id;
		this.name = name;
		this.zipCode = zipCode;
		this.country = country;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getZipCode() {
		return zipCode;
	}

	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}
	
	
	
}
