package vp.spring.Rest.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.spring.Rest.model.Country;
import vp.spring.Rest.model.Place;

@Component
public class PlaceRepository {

	CountryRepository countryRepository;
	
	private List<Place> places = new ArrayList<>();

	@Autowired
	public PlaceRepository(CountryRepository countryRepository) {
		places.add(new Place(1, "Novi Sad", 21000, countryRepository.findOne(1)));
		places.add(new Place(2,"Belgrade", 11000, countryRepository.findOne(1)));
		places.add(new Place(3, "Paris", 75000, countryRepository.findOne(2)));
		places.add(new Place(4, "Rome", 35000, countryRepository.findOne(3)));
		this.countryRepository = countryRepository;
	}
	
	public List<Place> findAll() {
		return places;
	}
	
	public Place findOne(int id) {
		for (Place place : places) {
			if(place.getId() == id) {
				return place;
			}
		}
		return null;
	}

	public List<Place> findByCountryId(int id){
		List<Place> retVal = new ArrayList<>();
		for (Place place : places) {
			if(place.getCountry().getId() == id) {
				retVal.add(place);
			}
		}
		return retVal;
	}
	
	public Place savePlace(Place place, int countryId) {
		Place existingPlace = findOne(place.getId());
		
		if (existingPlace == null) {
			Country country = countryRepository.findOne(countryId);
			place.setCountry(country);
			place.setId(places.size()+1);
			places.add(place);
		} else {
			existingPlace.setName(place.getName());
			existingPlace.setZipCode(place.getZipCode());
			existingPlace.setCountry(countryRepository.findOne(countryId));
		}
		return place;
	}
	
	public void deletePlace(int id)  {
		Iterator<Place> it = places.iterator();
		while (it.hasNext()) {
			if (it.next().getId() == id) {
				it.remove();
				return;
			}
		}

	}
	
	public List<Place> findByNameContains(String name) {
		List<Place> retVal = new ArrayList<Place>();
		for (Place place: places) {
			if (place.getName().contains(name)) {
				retVal.add(place);
			}
		}
		return retVal;
	}
	
	public Place findByZipCode(int zipCode) {
		for (Place place: places) {
			if (place.getZipCode() == zipCode) {
				return place;
			}
		}
		return null;
	}

}
