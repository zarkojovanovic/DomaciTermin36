package vp.rest.student.RESTStudent.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.rest.student.RESTStudent.model.Course;
import vp.rest.student.RESTStudent.model.Exam;
import vp.rest.student.RESTStudent.service.CourseService;
import vp.rest.student.RESTStudent.service.ExamService;

@RestController
public class CourseController {
	@Autowired
	CourseService courseService;
	@Autowired
	ExamService examService;
	
	@RequestMapping(value="api/courses", method= RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<List<Course>> getAllCourses() {
		List<Course> courses = courseService.findAll();
		
		return new ResponseEntity<>(courses, HttpStatus.OK);
	}
	
	@RequestMapping(value="api/courses/{id}", method = RequestMethod.GET)
	public ResponseEntity<Course> getCourse(@PathVariable int id) {
		Course course = courseService.findOne(id);
		
		return new ResponseEntity<>(course, HttpStatus.OK);
	}
	
	@RequestMapping(value="api/courses", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Course> create(@RequestBody Course course) {
		Course retVal = courseService.save(course);
		
		return new ResponseEntity<>(retVal, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="api/courses/{id}", method = RequestMethod.PUT,  consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Course> update(@PathVariable int id, @RequestBody Course course) {
		course.setId(id);
		Course retVal = courseService.save(course);
		
		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}
	
	@RequestMapping(value="api/courses/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable int id) {
		Course course = courseService.findOne(id);
		if (course != null) {
			courseService.remove(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} else  {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="api/courses", method = RequestMethod.GET, params =
			"name")
	public ResponseEntity<List<Course>> getByName(@RequestParam String name) {
		List<Course> retVal = courseService.findByNameContains(name);
		
		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}
	
	@RequestMapping(value="api/courses", method = RequestMethod.GET, params = "espb")
	public ResponseEntity<List<Course>> getByEspb(@RequestParam int espb) {
		List<Course> retVal = courseService.findByEspb(espb);
		
		return new ResponseEntity<>(retVal, HttpStatus.OK );
	}
	
	@RequestMapping(value="api/courses/exams", method = RequestMethod.GET, params = "courseName")
	public ResponseEntity<List<Exam>> getByCourseName(@RequestParam String courseName) {
		List<Exam> retVal = examService.findByCourseName(courseName);
		
		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}
	
	@RequestMapping(value="api/courses/exams/average", method = RequestMethod.GET, params = "courseName")
	public ResponseEntity<String> getAverageGradeOfCourse(@RequestParam String courseName) {
		double avg = examService.getAverageGradOfCourse(courseName);
		Course course = courseService.findByName(courseName);
		String response = course + " has an average pass grade of: " + String.format("%10.2f", avg);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	

}