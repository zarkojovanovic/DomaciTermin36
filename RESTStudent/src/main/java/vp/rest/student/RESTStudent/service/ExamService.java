package vp.rest.student.RESTStudent.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.rest.student.RESTStudent.data.ExamRepository;
import vp.rest.student.RESTStudent.model.Exam;



@Component
public class ExamService {
	@Autowired
	ExamRepository examRepository;

	public List<Exam> findAll() {
		return examRepository.findAll();
	}

	public Exam findOne(int id) {
		return examRepository.findOne(id);
	}

	public Exam save(Exam exam) {
		examRepository.save(exam);
		// snimanje u fajl pri svakoj izmeni
		try {
			persistExams();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return exam;
	}

	public void remove(int id) {
		examRepository.delete(id);
		// snimanje u fajl pri svakoj izmeni
		try {
			persistExams();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public List<Exam> findByStudentId(int studentId) {
		return examRepository.findByStudentId(studentId);
	}
	
	public List<Exam> findByCourseId(int courseId) {
		return examRepository.findByCourseId(courseId);
	}
	
	public List<Exam> findByStudentCardNumber(String cardNumber) {
		return examRepository.findByStudentCardNumber(cardNumber);
	}
	
	public List<Exam> findByCourseName(String name) {
		return examRepository.findByCourseName(name);
	}
	
	public double getAverageGradeOfStudent(String cardNumber) {
		int sum =0;
		List<Exam> exams = findByStudentCardNumber(cardNumber);
		for (Exam exam: exams) {
			sum+=exam.getGrade();
		}
		return (double)sum / exams.size();
	}
	
	public double getAverageGradOfCourse(String name) {
		int sum = 0;
		List<Exam> exams = findByCourseName(name);
		for (Exam exam: exams) {
			sum+=exam.getGrade();
		}
		return (double)sum / exams.size(); 
	}

	public void persistExams() throws IOException {
		examRepository.saveToFile();
	}
}
