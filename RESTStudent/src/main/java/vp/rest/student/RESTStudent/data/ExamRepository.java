package vp.rest.student.RESTStudent.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.rest.student.RESTStudent.model.Exam;
import vp.rest.student.RESTStudent.service.CourseService;
import vp.rest.student.RESTStudent.service.StudentService;

@Component
public class ExamRepository {
	private List<Exam> exams;
	@Autowired
	StudentService studentService;
	@Autowired
	CourseService courseService;
	
	@Autowired
	public ExamRepository(StudentRepository studentRepository, CourseRepository courseRepository) throws IOException {
		// ucitavamo ispite iz fajla pri instanciranju repozitorijuma
		BufferedReader inputStream = new BufferedReader(new FileReader("data/exams.txt"));
		exams = new ArrayList<Exam>();

        String line;
		while ((line = inputStream.readLine()) != null) {
			String[] data = line.split(",");
			int id = Integer.valueOf(data[0]);
			int studentId = Integer.valueOf(data[1]);
			int courseId = Integer.valueOf(data[2]);
			int grade = Integer.valueOf(data[3]);
			
			// studenta i predmet dobijamo iz njihovih repozitorijuma preko id-a
			Exam exam = new Exam(id, studentRepository.findOne(studentId), 
					courseRepository.findOne(courseId), grade);
			
			exams.add(exam);
		} 
		inputStream.close();
	}
	
	public List<Exam> findAll() {
		return exams;
	}
	
	public Exam findOne(int id) {
		for (Exam exam: exams) {
			if (exam.getId() == id) {
				return exam;
			}
		}
		return null;
	}
	
	public List<Exam> findByStudentId(int studentId) {
		List<Exam> retVal = new ArrayList<>();
		for (Exam exam: exams) {
			if (exam.getStudent().getId() == studentId) {
				retVal.add(exam);
			}
		}
		return retVal;
	}
	
	public List<Exam> findByStudentCardNumber(String cardNumber) {
		List<Exam> retVal = new ArrayList<>();
		for (Exam exam: exams) {
			if (exam.getStudent().getCardNumber().equals(cardNumber)) {
				retVal.add(exam);
			}
		}
		return retVal;
	}
	
	public List<Exam> findByCourseName(String name) {
		List<Exam> retVal = new ArrayList<>();
		for(Exam exam: exams) {
			if (exam.getCourse().getName().equalsIgnoreCase(name)) {
				retVal.add(exam);
			}
		}
		return retVal;
	}
 	
	public List<Exam> findByCourseId(int courseId) {
		List<Exam> retVal = new ArrayList<>();
		for (Exam exam: exams) {
			if (exam.getCourse().getId() == courseId) {
				retVal.add(exam);
			}
		}
		return retVal;
	}
	
	public Exam save(Exam exam) {
		Exam existingExam = findOne(exam.getId());
		if (existingExam == null) {
		exam.setId(exams.size() + 1);
		exam.setStudent(studentService.findOne(exam.getStudent().getId()));
		exam.setCourse(courseService.findOne(exam.getCourse().getId()));
		exams.add(exam);
		} else {
			existingExam.setCourse(exam.getCourse());
			existingExam.setStudent(exam.getStudent());
			existingExam.setGrade(exam.getGrade());
		}
		return exam;
	}
	
	public void delete(int id) {		
		Iterator<Exam> it = exams.iterator();
		while (it.hasNext()) {
			if (it.next().getId() == id) {
				it.remove();
				return;
			}
		}			
	}
	
	public void saveToFile() throws IOException  {
		PrintWriter stream = new PrintWriter(new FileWriter("data/exams.txt"));

		for (Exam exam : exams) {
			stream.println(exam.getId() + "," + exam.getStudent().getId() + "," + 
					exam.getCourse().getId() + "," + exam.getGrade());				
		}
		stream.close();
	}
}
