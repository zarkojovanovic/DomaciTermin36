package vp.rest.student.RESTStudent.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.rest.student.RESTStudent.data.CourseRepository;
import vp.rest.student.RESTStudent.model.Course;

@Component
public class CourseService {
	@Autowired
	CourseRepository courseRepository;
	
	@Autowired
	ExamService examService;

	public List<Course> findAll() {
		return courseRepository.findAll();
	}

	public Course findOne(int id) {
		return courseRepository.findOne(id);
	}

	public Course save(Course course) {
		 courseRepository.save(course);
		// snimanje u fajl pri svakoj izmeni
		try {
			persistCourses();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return course;
	}

	public void remove(int id) {
		// dozvoljeno brisanje samo predmeta za koje ne postoje ispiti
		if (examService.findByCourseId(id).isEmpty()) {
			courseRepository.delete(id);
		}
		// snimanje u fajl pri svakoj izmeni
		try {
			persistCourses();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public List<Course> findByNameContains(String name) {
		return courseRepository.findByNameContains(name);
	}
	
	public Course findByName(String name) {
		return courseRepository.findByName(name);
	}
	
	public List<Course> findByEspb(int espb) {
		return courseRepository.findByEspb(espb);
	}

	public void persistCourses() throws IOException {
		courseRepository.saveToFile();
	}
}
